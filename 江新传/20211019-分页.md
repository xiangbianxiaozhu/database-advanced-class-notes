# 分页

## 语句：

```sql
select * from admintable 

--要分页的表

order by id asc

--分页前必须要排序

offset 10 rows fetch next 10 rows only

--分页语句

```
## 结果：

![ll](./imgs/001.png)