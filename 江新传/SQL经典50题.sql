create database ClassicDb
GO

use ClassicDb
GO

create table StudentInfo
(
    Id int PRIMARY key not null IDENTITY,
    StudentCode nvarchar(80),
    StudentName nvarchar(80),
    Birthday date not null,
    Sex nvarchar(2),
    ClassId int not null
)

GO

-- select * from StudentInfo

insert into StudentInfo (StudentCode,StudentName,Birthday,Sex,ClassId) values('01' , '赵雷' , '1990-01-01' , 'm',1)
insert into StudentInfo (StudentCode,StudentName,Birthday,Sex,ClassId) values('02' , '钱电' , '1990-12-21' , 'm',1)
insert into StudentInfo (StudentCode,StudentName,Birthday,Sex,ClassId) values('03' , '孙风' , '1990-12-20' , 'm',1)
insert into StudentInfo (StudentCode,StudentName,Birthday,Sex,ClassId) values('04' , '李云' , '1990-12-06' , 'm',1)
insert into StudentInfo (StudentCode,StudentName,Birthday,Sex,ClassId) values('05' , '周梅' , '1991-12-01' , 'f',1)
insert into StudentInfo (StudentCode,StudentName,Birthday,Sex,ClassId) values('06' , '吴兰' , '1992-01-01' , 'f',1)
insert into StudentInfo (StudentCode,StudentName,Birthday,Sex,ClassId) values('07' , '郑竹' , '1989-01-01' , 'f',1)
insert into StudentInfo (StudentCode,StudentName,Birthday,Sex,ClassId) values('09' , '张三' , '2017-12-20' , 'f',1)
insert into StudentInfo (StudentCode,StudentName,Birthday,Sex,ClassId) values('10' , '李四' , '2017-12-25' , 'f',1)
insert into StudentInfo (StudentCode,StudentName,Birthday,Sex,ClassId) values('11' , '李四' , '2012-06-06' , 'f',1)
insert into StudentInfo (StudentCode,StudentName,Birthday,Sex,ClassId) values('12' , '赵六' , '2013-06-13' , 'f',1)
insert into StudentInfo (StudentCode,StudentName,Birthday,Sex,ClassId) values('13' , '孙七' , '2014-06-01' , 'f',1)


GO


CREATE TABLE Teachers
(
    Id int PRIMARY key not null IDENTITY,
    TeacherName nvarchar(80)
)

go
-- select * from Teachers

insert into Teachers (TeacherName) values('张三')
insert into Teachers (TeacherName) values('李四')
insert into Teachers (TeacherName) values('王五')

GO

create table CourseInfo
(
    Id int PRIMARY key not null IDENTITY,
    CourseName NVARCHAR(80) not null,
    TeacherId int not null
)

go
-- select * from CourseInfo

insert into CourseInfo (CourseName,TeacherId) values( '语文' , 2)
insert into CourseInfo (CourseName,TeacherId) values( '数学' , 1)
insert into CourseInfo (CourseName,TeacherId) values( '英语' , 3)

GO

create table StudentCourseScore
(
    Id int PRIMARY key not null IDENTITY,
    StudentId int not null,
    CourseId int not null,
    Score int not null
)
go
-- select * from StudentCourseScore

insert into StudentCourseScore (StudentId,CourseId,Score) values((select Id from StudentInfo where StudentCode='01') , 1 , 80)
insert into StudentCourseScore (StudentId,CourseId,Score) values((select Id from StudentInfo where StudentCode='01') , 2 , 90)
insert into StudentCourseScore (StudentId,CourseId,Score) values((select Id from StudentInfo where StudentCode='01') , 3 , 99)
insert into StudentCourseScore (StudentId,CourseId,Score) values((select Id from StudentInfo where StudentCode='02') , 1 , 70)
insert into StudentCourseScore (StudentId,CourseId,Score) values((select Id from StudentInfo where StudentCode='02') , 2 , 60)
insert into StudentCourseScore (StudentId,CourseId,Score) values((select Id from StudentInfo where StudentCode='02') , 3 , 80)
insert into StudentCourseScore (StudentId,CourseId,Score) values((select Id from StudentInfo where StudentCode='03') , 1 , 80)
insert into StudentCourseScore (StudentId,CourseId,Score) values((select Id from StudentInfo where StudentCode='03') , 2 , 80)
insert into StudentCourseScore (StudentId,CourseId,Score) values((select Id from StudentInfo where StudentCode='03') , 3 , 80)
insert into StudentCourseScore (StudentId,CourseId,Score) values((select Id from StudentInfo where StudentCode='04') , 1 , 50)
insert into StudentCourseScore (StudentId,CourseId,Score) values((select Id from StudentInfo where StudentCode='04') , 2 , 30)
insert into StudentCourseScore (StudentId,CourseId,Score) values((select Id from StudentInfo where StudentCode='04') , 3 , 20)
insert into StudentCourseScore (StudentId,CourseId,Score) values((select Id from StudentInfo where StudentCode='05') , 1 , 76)
insert into StudentCourseScore (StudentId,CourseId,Score) values((select Id from StudentInfo where StudentCode='05') , 2 , 87)
insert into StudentCourseScore (StudentId,CourseId,Score) values((select Id from StudentInfo where StudentCode='06') , 1 , 31)
insert into StudentCourseScore (StudentId,CourseId,Score) values((select Id from StudentInfo where StudentCode='06') , 3 , 34)
insert into StudentCourseScore (StudentId,CourseId,Score) values((select Id from StudentInfo where StudentCode='07') , 2 , 89)
insert into StudentCourseScore (StudentId,CourseId,Score) values((select Id from StudentInfo where StudentCode='07') , 3 , 98)

go



select * from CourseInfo

select * from StudentCourseScore

select * from StudentInfo

select * from Teachers



-- 练习题目：

-- 1.查询"数学 "课程比" 语文 "课程成绩高的学生的信息及课程分数

;with CTE_A
as
(
	select StudentId,
	(case when CourseId =1 then Score end ) Chinese,
	(case when CourseId =2 then Score end ) Math,
	(case when CourseId =3 then Score end ) English
	from StudentCourseScore


),CTE_B as
(
select StudentId,sum(Chinese) chainse ,sum(Math) math,sum(English) english from CTE_A group by StudentId



)select * from CTE_B where math>chainse


-- 1.1 查询同时存在" 数学 "课程和" 语文 "课程的情况

select  a.StudentId ,a.Score ,b.Score from StudentCourseScore a left join StudentCourseScore b 

on a.StudentId =b.StudentId

where a.CourseId=1 and b.CourseId=2



;with CTE_A
as
(
	select StudentId,
	  Score Chinese
	from StudentCourseScore where CourseId=1


),CTE_B as
(


select CTE_A.StudentId,Chinese ,Score Math from CTE_A ,StudentCourseScore
 where StudentCourseScore.CourseId=2 and CTE_A.StudentId=StudentCourseScore.StudentId

)select * from CTE_B 



-- 1.2 查询存在" 数学 "课程但可能不存在" 语文 "课程的情况(不存在时显示为 null )

;with ATE_A

as(
	select StudentId , 

	(case  when CourseId=1 then Score end   ) chinese
	
	
	from StudentCourseScore


),
ATE_B

as(

   select StudentId ,sum(chinese) chinese from ATE_A group by StudentId

)select a.* , b.Score math from ATE_B a,StudentCourseScore b where a.StudentId=b.StudentId and b.CourseId=2



-- 1.3 查询不存在" 数学 "课程但存在" 语文 "课程的情况

select a.*,b.Score from 
(select * from StudentCourseScore where StudentId not in(
select StudentId from StudentCourseScore where CourseId=2) and CourseId=1)a
left join
(select StudentId,Score from StudentCourseScore where CourseId=3) b on a.StudentId=b.StudentId


-- 2.查询平均成绩大于等于 60 分的同学的学生编号和学生姓名和平均成绩
 
	select StudentId,avg(Score) 平均成绩 from StudentCourseScore group by StudentId having avg(Score)>60

-- 3.查询在 成绩 表存在成绩的学生信息

	select * from StudentInfo where id in (select StudentId from StudentCourseScore) 


-- 4.查询所有同学的学生编号、学生姓名、选课总数、所有课程的总成绩(没成绩的显示为 null )

select b.Id,StudentName ,count(a.CourseId) 选课总数 ,sum(a.Score) 总成绩  

from StudentInfo b 

left join StudentCourseScore a on b.Id=a.StudentId 

group by b.Id ,StudentName


-- 4.1 查有成绩的学生信息

select * from StudentInfo 

where id in (

select  StudentId 

from StudentCourseScore

)


-- 5.查询「李」姓老师的数量


select id , count(TeacherName) 数量 from Teachers where TeacherName like '李%' group by id



-- 6.查询学过「张三」老师授课的同学的信息


;with CTE_A
as
(
	select  
		
		(case when TeacherName='张三' then Id end  ) id
	
	from Teachers


), CTE_B  as

(

		select sum(id) id from CTE_A 
 
)
 select * 
 
	from StudentInfo 
 
	where id in(

 select  StudentId 
 
	from StudentCourseScore 
 
	where CourseId in( 
 
 select * from CTE_B

)
)



-- 7.查询没有学全所有课程的同学的信息


select StudentId   from StudentCourseScore  group by StudentId having count(CourseId)<3



-- 8.查询至少有一门课与学号为" 01 "的同学所学相同的同学的信息

	select * from StudentCourseScore 


-- 9.查询和" 01 "号的同学学习的课程 完全相同的其他同学的信息


;with CTE_A
as
(
	select StudentId,

	(case when CourseId =1 then CourseId end ) Chinese,

	(case when CourseId =2 then CourseId end ) Math,

	(case when CourseId =3 then CourseId end ) English

	from StudentCourseScore


),CTE_B as
(
	select StudentId,sum(Chinese) chainse ,sum(Math) math,sum(English) english 

	from CTE_A 

	group by StudentId


)

select * from StudentInfo where id in(

select b.StudentId 

from (select * from CTE_B where StudentId =1) a,(select * from CTE_B where StudentId!=1) b 

where a.math=b.math and a.english=b.english and a.chainse=b.chainse 

)



-- 10.查询没学过"张三"老师讲授的任一门课程的学生姓名


select StudentId 

	from StudentCourseScore  

	where  StudentId not in(

select StudentId 

	from StudentCourseScore 

	where CourseId in  (

select Id 
	
	from Teachers 
	
	where TeacherName='张三'

)
) 
	group by StudentId


-- 11.查询两门及其以上不及格课程的同学的学号，姓名及其平均成绩

;with CTE_A as
(

select 

		StudentId,

		(case when Score>=60 then 1 else 0 end ) 及格

	from StudentCourseScore

), CTE_B as 

(

select 

			StudentId,
			
			count(及格) 及格  


	from CTE_A

group by StudentId
)
select StudentId from CTE_B where 及格>=2 



-- 12.检索" 数学 "课程分数小于 60，按分数降序排列的学生信息

select *  from StudentInfo b right join (
select StudentId ,RANK() over (  order by score desc  ) 排名 from StudentCourseScore where CourseId=2 and Score<60
) a on a.StudentId=b.Id

-- 13.按平均成绩从高到低显示所有学生的所有课程的成绩以及平均成绩
select Id ,b.* from StudentInfo a left join( 

SELECT 
	   
	   StudentId , 
	   
	   avg(Score) AvgScore ,
		
	   sum(case when CourseId=1 then Score end ) chinese, 
	   
	   sum(case when CourseId=2 then Score end ) math, 
	   
	   sum(case when CourseId=3 then Score end ) english  
	   
	from StudentCourseScore 

group by StudentId  

) b on a.Id=b.StudentId order by avgscore desc

-- 14.查询各科成绩最高分、最低分和平均分：

-- 15.以如下形式显示：课程 ID，课程 name，最高分，最低分，平均分，及格率，中等率，优良率，优秀率
/*
	及格为>=60，中等为：70-80，优良为：80-90，优秀为：>=90

	要求输出课程号和选修人数，查询结果按人数降序排列，若人数相同，按课程号升序排列

	按各科成绩进行排序，并显示排名， Score 重复时保留名次空缺
*/	
	
		   --sum(case when Score>70 and Score<=80 then Score end) 中等率,

		   --sum(case when Score>80 and Score<=90 then Score end) 优良率,
		   
		   --sum(case when Score>90 then Score end) 优秀率
	
	     

-- 15.1 按各科成绩进行排序，并显示排名， Score 重复时合并名次

	select *,RANK() over (partition by courseid order by score desc)  from StudentCourseScore

-- 16.查询学生的总成绩，并进行排名，总分重复时保留名次空缺


	select *,DENSE_RANK() over (partition by courseid order by score desc)  from StudentCourseScore


-- 16.1 查询学生的总成绩，并进行排名，总分重复时不保留名次空缺


select StudentId,sum(Score) SumScore,RANK() over (order by sum(Score) desc)   from StudentCourseScore group by StudentId

-- 17.统计各科成绩各分数段人数：课程编号，课程名称，[100-85]，[85-70]，[70-60]，[60-0] 及所占百分比

select CourseName,a.* from CourseInfo b right join (

select 
		CourseId,

	    CONVERT (nvarchar(80), CONVERT (decimal (18,2), sum(case when score>=85 then 1 else 0 end)*100.0/count(*)))+'%' [100-85],
		
		CONVERT(nvarchar(80),CONVERT(decimal(18,2) , sum(case when score<85 and score>=70 then 1 else 0 end)*100.0/count(*)))+'%'  [85-70],

		CONVERT(nvarchar(80),CONVERT(decimal(18,2) , sum(case when score<70 and score>=60 then 1 else 0 end)*100.0/count(*)))+'%' [70-60],

		CONVERT(nvarchar(80),CONVERT(decimal(18,2) , sum(case when score<60 and score>=0 then 1 else 0 end)*100.0/count(*)))+'%' [60-0]

	from StudentCourseScore

group by CourseId) a on a.CourseId=b.Id


-- 18.查询各科成绩前三名的记录

select a.数学 ,b.语文 ,c.英语 from (

select top 3 Score 数学,ROW_NUMBER() over (order by score desc) ph 

	from StudentCourseScore 

	where CourseId=2  

) a left join (

select top 3 Score 语文,ROW_NUMBER() over (order by score desc) ph  

	from StudentCourseScore 
	
	where CourseId=1 order by Score desc ) b 
	
	on a.ph =b.ph

left join (

select top 3 Score 英语,ROW_NUMBER() over (order by score desc) ph  

	from StudentCourseScore 
	
	where CourseId=3 order by Score desc 
 
 ) c on b.ph=c.ph


-- 19.查询每门课程被选修的学生数

 select 
 
		sum (case when CourseId=1 then 1 else 0 end ) 语文,


		sum (case when CourseId=2 then 1 else 0 end ) 数学,


		sum (case when CourseId=3 then 1 else 0 end ) 英语

 
   from StudentCourseScore



-- 20.查询出只选修两门课程的学生学号和姓名

select 

		StudentName ,Id 

	from StudentInfo 

	where Id in(

select 

		StudentId 

  from StudentCourseScore

  group by StudentId 

  having count(CourseId)=2

)


-- 21.查询男生、女生人数


select 

		sum (case when Sex='f' then 1 else 0 end  ) 女生人数,

		sum (case when Sex='m' then 1 else 0 end  ) 男生人数

	from StudentInfo
	


-- 22.查询名字中含有「风」字的学生信息

select * from StudentInfo where StudentName like '%风%'


-- 23.查询同名同性学生名单，并统计同名人数


select 
		StudentName,

		count(StudentName) 同名人数


	from StudentInfo
	
	group by StudentName	



-- 24.查询 1990 年出生的学生名单

select * from  StudentInfo where Birthday like '1990%'

-- 25.查询每门课程的平均成绩，结果按平均成绩降序排列，平均成绩相同时，按课程编号升序排列

select 

		CourseId,
		
		avg(score) 
		
	from StudentCourseScore 
	
	group by CourseId 
	
	order by avg(Score) desc ,
			
			 CourseId



-- 26.查询平均成绩大于等于 85 的所有学生的学号、姓名和平均成绩
select StudentName,a.* from StudentInfo right join (

select  StudentId , avg(score) avgscore from StudentCourseScore  group by StudentId  having avg(score)>85 ) a on a.StudentId=StudentInfo.Id


-- 27.查询课程名称为「数学」，且分数低于 60 的学生姓名和分数

select 

		StudentName ,b.数学成绩 
	
	from StudentInfo a 
	
	right join (

select 

		Score 数学成绩,
		
		Id  
	
	from StudentCourseScore 
	
	where Score<60 and  CourseId 
	
	in (

select 

		Id 
		
	from CourseInfo 
	
	where CourseName like '数学'

)
) b 

	on a.Id= b.Id


-- 28.查询所有学生的课程及分数情况（存在学生没成绩，没选课的情况）

select Id ,chinese,math,english from StudentInfo a left join (

select 
		
		StudentId,
		
		sum (case when CourseId=1 then score end ) chinese ,

		sum (case when CourseId=2 then score end ) math ,

		sum (case when CourseId=3 then score end ) english 

	from StudentCourseScore 

	group by StudentId) b on a.Id=b.StudentId




-- 29.查询任何一门课程成绩在 70 分以上的姓名、课程名称和分数



select a.StudentName,b.Score from StudentInfo a left join (select * from CourseInfo ) c on a.StudentCode  left join (

select *  from StudentCourseScore where Score>70

) b on a.Id=b.StudentId 


-- 30.查询不及格的课程

-- 31.查询课程编号为 01 且课程成绩在 80 分以上的学生的学号和姓名

-- 32.求每门课程的学生人数

-- 33.成绩不重复，查询选修「张三」老师所授课程的学生中，成绩最高的学生信息及其成绩

--34.成绩有重复的情况下，查询选修「张三」老师所授课程的学生中，成绩最高的学生信息及其成绩

-- 35.查询不同课程成绩相同的学生的学生编号、课程编号、学生成绩

-- 36.查询每门功成绩最好的前两名

-- 37.统计每门课程的学生选修人数（超过 5 人的课程才统计）。

-- 38.检索至少选修两门课程的学生学号

-- 39.查询选修了全部课程的学生信息

-- 40.查询各学生的年龄，只按年份来算

-- 41.按照出生日期来算，当前月日 < 出生年月的月日则，年龄减一

-- 42.查询本周过生日的学生

-- 43.查询下周过生日的学生

-- 44.查询本月过生日的学生

-- 45.查询下月过生日的学生
